package com.example.uibatch.processor;

import com.example.uibatch.model.SalesRecord;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

/**
 * Created by iecan on 2/08/2018.
 */
@Component
public class SalesRecordProcessor implements ItemProcessor<SalesRecord, SalesRecord> {
    @Override
    public SalesRecord process(SalesRecord salesRecord) throws Exception {
        salesRecord.setProductName(salesRecord.getProductName().toUpperCase());
        salesRecord.setSalesPerson(salesRecord.getSalesPerson().toUpperCase());
        return salesRecord;
    }
}
