package com.example.uibatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UibatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(UibatchApplication.class, args);
	}
}
