package com.example.uibatch.model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by iecan on 30/07/2018.
 */
@Entity
public class SalesRecord {
    @Id
    private Integer id;
    private String productName;
    private String salesPerson;
    private String code;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(String salesPerson) {
        this.salesPerson = salesPerson;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
