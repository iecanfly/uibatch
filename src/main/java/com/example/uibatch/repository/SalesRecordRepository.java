package com.example.uibatch.repository;

import com.example.uibatch.model.SalesRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by iecan on 3/08/2018.
 */
public interface SalesRecordRepository extends PagingAndSortingRepository<SalesRecord, Integer> {
}
