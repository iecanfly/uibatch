package com.example.uibatch.service;

import com.example.uibatch.model.SalesRecord;
import com.example.uibatch.repository.SalesRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by iecan on 3/08/2018.
 */
@Component
public class SalesRecordService {

    @Autowired
    private SalesRecordRepository salesRecordRepository;

    public void saveAll(List<? extends SalesRecord> salesRecords) {
        salesRecordRepository.saveAll(salesRecords);
    }
}
