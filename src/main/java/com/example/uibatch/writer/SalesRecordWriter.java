package com.example.uibatch.writer;

import com.example.uibatch.model.SalesRecord;
import com.example.uibatch.service.SalesRecordService;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by iecan on 3/08/2018.
 */
@Component
public class SalesRecordWriter implements ItemWriter<SalesRecord> {

    @Autowired
    SalesRecordService salesRecordService;

    @Override
    public void write(List<? extends SalesRecord> items) throws Exception {
        salesRecordService.saveAll(items);
    }
}
